const specialChars = /[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/;
const number = /[0-9]/;
var errors = ""
$("#submit").click(function() {
    $(errors).remove()
    errors = $("<div class='error'></div>")

    var success = true
    var username = $("#username").val()
    var password = $("#password").val()
    var comfirmPassword = $("#confirm-password").val()
    var phone = $("#tel").val()
    var mail = $("#mail").val()

    if (username !== "") {
        if (username.length < 8) {
            showError("Identifiant trop court")
            success = false
        }
    } else {
        showError("Identifiant obligatoire")
        success = false
    }
    // if (success !== false) {
        if (password !== "") {
            if (password.length < 8) {
                showError("Mot de passe trop court trop court")
                success = false
            }
            if (!password.match(specialChars)) {
                showError("Le mot de passe doit contenir au moin un caractère spécial")
                success = false
            }
            if (!password.match(number)) {
                showError("Le mot de passe doit contenir au moins un chiffre")
                success = false
            }
        } else {
            showError("mot de passe obligatoire")
            success = false
        }
    // }
    // if (success !== false) {
        if (password !== comfirmPassword) {
            showError("Les mots de passe ne correspondent pas")
        }
    // }
    // if (success !== false) {
        if (phone !== "") {
            if (isNaN(phone)) {
                showError("Le téléphone est invalide")
                success = false
            } else if (phone.length !== 10) {
                showError("Le téléphone est invalide")
                success = false
            }
        } else {
            showError("numéro de téléphone obligatoire")
            success = false
        }
    // }
    // if (success !== false) {
        if (mail !== "") {
            var mailArray = mail.split('')
            var at = mailArray.indexOf("@")
            var dot = mailArray.indexOf(".")

            if(at !== -1 && dot !== -1 && dot > at ) {
                console.log("c'est good");
            } else {
                showError("Le mail est invalide");
                success = false
            }
        } else {
            showError("Le mail est obligatoire");
            success = false
        }
    // }
    if (success === true) {
        var success = $("<p class='success'>compte créé</p>")
        $("body").append(success)
    }
})

function showError(event) {
    let error = $("<p>"+event+"</p>")
    $("div").before(errors)
    $(errors).append(error)
}